from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search


def getLastTime(page, order):
    es = Elasticsearch()
    time = 0
    s = Search(using= es, index="uptimerobot") \
        .query("match", app=page) \
        .sort({"date" : {"order" : order}})
    s = s[0:1] 
    response = s.execute()
    if response.hits.total.value != 0:
        time = response['hits']['hits'][0]['sort'][0]
    return time

